import { Paciente } from "./paciente";

export class SignosVitales2{
    public idSignosVitales: number;
    public nombre_completo: string;
    public fecha: Date;
    public temperatura: string;
    public pulso: string;
    public ritmoRespiratorio: string;
}