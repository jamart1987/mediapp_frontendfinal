import { Paciente } from "./paciente";

export class SignosVitales{
    public idSignosVitales: number;
    public paciente: Paciente;
    public fecha: Date;
    public temperatura: string;
    public pulso: string;
    public ritmoRespiratorio: string;
}