import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Paciente } from '../../_model/paciente';
import { FormGroup, FormControl } from '@angular/forms';
import { PacienteService } from '../../_service/paciente.service';
import { SignosEdicionComponent } from '../signos-edicion/signos-edicion.component';

@Component({
  selector: 'app-nuevo-cliente',
  templateUrl: './nuevo-cliente.component.html',
  styleUrls: ['./nuevo-cliente.component.css']
})
export class NuevoClienteComponent implements OnInit {

  id: number;
  paciente: Paciente;
  form: FormGroup;
  edicion: boolean = false;


  constructor( public dialogRef: MatDialogRef<NuevoClienteComponent>, private pacienteService: PacienteService, @Inject(MAT_DIALOG_DATA) data) { 
     this.paciente = new Paciente();

       this.form = new FormGroup({
      'id': new FormControl(0),
      'nombres': new FormControl(''),
      'apellidos': new FormControl(''),
      'dni': new FormControl(''),
      'direccion': new FormControl(''),
      'telefono': new FormControl('')
    });

  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  operar() {
      console.log("no");
      this.paciente.nombres = this.form.value['nombres'];
      this.paciente.apellidos = this.form.value['apellidos'];
      this.paciente.dni = this.form.value['dni'];
      this.paciente.direccion = this.form.value['direccion'];
      this.paciente.telefono = this.form.value['telefono'];
      //insert
      this.pacienteService.registrar(this.paciente).subscribe(data => {
        if(data === 1) 
        this.pacienteService.mensaje.next('Se registró');
        else
        this.pacienteService.mensaje.next('No se registro');
        
        this.dialogRef.close();
      });
    
  }

  close() {
    console.log("llega");
    this.dialogRef.close({ data: '0' });
  }




  ngOnInit() {
  }

}
