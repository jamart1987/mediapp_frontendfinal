import { Component, OnInit } from '@angular/core';
import * as decode from 'jwt-decode';
import { TOKEN_NAME } from '../../_shared/var.constant';


@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.css']
})
export class UserInfoComponent implements OnInit {
  username: string
  role: string

  constructor() { 
    let tk = JSON.parse(sessionStorage.getItem(TOKEN_NAME));
    const decodedToken = decode(tk.access_token);
    console.log(decodedToken);
    this.username = decodedToken.user_name;
    this.role = decodedToken.authorities[0];
  }

  ngOnInit() {
  }

}
