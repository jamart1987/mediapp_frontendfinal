import { SignosVitales2 } from './../../_model/signosVitales2';
import { Paciente } from './../../_model/paciente';
import { SignosVitalesService} from './../../_service/signosVitales.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SignosVitales } from '../../_model/signosVitales';
import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar } from '@angular/material';
import { elementAt } from 'rxjs/operators';

@Component({
  selector: 'app-signos-vitales',
  templateUrl: './signos-vitales.component.html',
  styleUrls: ['./signos-vitales.component.css']
})
export class SignosVitalesComponent implements OnInit {

  lista: SignosVitales[] = [];
  Signos2: SignosVitales2[] = [];
  displayedColumns = ['idSignosVitales', 'Fecha', 'Paciente','temperatura','pulso','ritmoRespiratorio', 'acciones'];
  dataSource: MatTableDataSource<SignosVitales2>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  cantidad: number;


  constructor(public route: ActivatedRoute, private SignosVitalesService: SignosVitalesService, public snackBar: MatSnackBar) {

   }

   ngOnInit() {
    this.SignosVitalesService.getlistarSignosVitales(0, 10).subscribe(data => {
    
      let signos = JSON.parse(JSON.stringify(data)).content;
      this.cantidad = JSON.parse(JSON.stringify(data)).totalElements;
      
      
      this.dataSource = new MatTableDataSource(this.fixSignos(signos));
      //this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
    

    this.SignosVitalesService.SignosVitalesCambio.subscribe(data => {
      let Signos = JSON.parse(JSON.stringify(data)).content;
      this.cantidad = JSON.parse(JSON.stringify(data)).totalElements;

      
      this.dataSource = new MatTableDataSource(this.fixSignos(Signos));
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

    this.SignosVitalesService.mensaje.subscribe(data => {
      this.snackBar.open(data, null, { duration: 2000 });
    });


  }

  mostrarMas(e) {
    //e.pageIndex e.pageSize e.length
    //console.log(e.pageIndex);
    //console.log(e.pageSize);

    this.SignosVitalesService.getlistarSignosVitales(e.pageIndex, e.pageSize).subscribe(data => {
      console.log(data);
      let Signos = JSON.parse(JSON.stringify(data)).content;
      this.cantidad = JSON.parse(JSON.stringify(data)).totalElements;

      this.dataSource= new MatTableDataSource( this.fixSignos(Signos));
      //this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      
    });
  }

  fixSignos(signos:any){
    this.Signos2 = [];
    signos.forEach(element => {
      let signos2 = new SignosVitales2();
      signos2.idSignosVitales = element.idSignosVitales;
      signos2.fecha = element.fecha;
      signos2.temperatura = element.temperatura;
      signos2.pulso = element.pulso;
      signos2.ritmoRespiratorio = element.ritmoRespiratorio;
      signos2.nombre_completo = element.paciente.nombres + " " + element.paciente.apellidos;
      
      this.Signos2.push(signos2);
    });

    return this.Signos2;

  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;

    
  }


}
