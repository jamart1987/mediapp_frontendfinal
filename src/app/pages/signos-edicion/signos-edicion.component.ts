import { Paciente } from './../../_model/paciente';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { SignosVitalesService } from './../../_service/signosVitales.service';
import { SignosVitales } from './../../_model/signosVitales';
import { NuevoClienteComponent } from './../nuevo-cliente/nuevo-cliente.component';
import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { PacienteService } from '../../_service/paciente.service';
import { map, startWith } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { MatDialog, MatDialogRef, MatSnackBar, MatAutocomplete } from '@angular/material';

@Component({
  selector: 'app-signos-edicion',
  templateUrl: './signos-edicion.component.html',
  styleUrls: ['./signos-edicion.component.css']
})
export class SignosEdicionComponent implements OnInit {

  myControl: FormControl = new FormControl();
  myControlMedico: FormControl = new FormControl();

  pacientes: Paciente[] = [];
  SignosVitales: SignosVitales;
  form: FormGroup;
  edicion: boolean = false;
 
  pacienteSeleccionado: Paciente;
  filteredOptions: Observable<any[]>;

  idPacienteSeleccionado: number;
  fecha:Date = new Date();
  temperatura: string;
  pulso: string;
  ritmo_respiratorio: string;
  id:number;
  fechaSeleccionada: Date = new Date();
  @ViewChild(MatAutocomplete) matAutocomplete: MatAutocomplete;
  PacienteEdit: Paciente;
  

  constructor(private route: ActivatedRoute, private Router:Router,private SignosVitalesService:SignosVitalesService, private pacienteService: PacienteService, public dialog: MatDialog, public snackBar: MatSnackBar) {
        this.SignosVitales = new SignosVitales();
        
        this.form = new FormGroup({
          'id': new FormControl(0),
          'Paciente': new FormControl(new Paciente()),
          'fecha': new FormControl(new Date()),
          'temperatura': new FormControl(''),
          'pulso': new FormControl(''),
          'ritmo_respiratorio': new FormControl('')
        });
      }
      
      openDialog(): void {
        let dialogRef = this.dialog.open(NuevoClienteComponent, {
        width: '450px',
        data: {  }
      });
  
      dialogRef.afterClosed().subscribe(result => {
         this.listarPacientes();
      });
      }
  

  ngOnInit() {

    this.fecha.setHours(0);
    this.fecha.setMinutes(0);
    this.fecha.setSeconds(0);
    this.fecha.setMilliseconds(0);
 
    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = params['id'] != null;
      this.initForm();
    });

    this.listarPacientes();
    
    this.filteredOptions = this.myControl.valueChanges
    .pipe(
      startWith(null),
      map(val => this.filter(val))
    );

  }

  private initForm() {
    if (this.edicion) {
      this.SignosVitalesService.getSignosVitalesPorId(this.id).subscribe(data => {
        let id = data.idSignosVitales;
        let paciente = data.paciente;
        let fecha =   data.fecha.toString();
        let pulso = data.pulso;
        let ritmoRespiratorio = data.ritmoRespiratorio;
        let temperatura = data.temperatura;
        
        this.pacienteSeleccionado = paciente;
        this.PacienteEdit = paciente;
        
        this.form = new FormGroup({
          'id': new FormControl(id),
         // 'Paciente': new FormControl(paciente),
          'fecha': new FormControl(new Date(fecha)),
          'temperatura': new FormControl(pulso),
          'pulso': new FormControl(ritmoRespiratorio),
          'ritmo_respiratorio': new FormControl(temperatura)
        });
      });
    }
  }

  displayFn(val: Paciente) {
    return val ? `${val.nombres} ${val.apellidos}` : val;
  }

  seleccionarPaciente(e) {
    this.pacienteSeleccionado = e.option.value;
  }

  listarPacientes() {
    this.pacienteService.getlistar().subscribe(data => {
      this.pacientes = data;
    });
  }

  aceptar() {   
    this.SignosVitales.idSignosVitales = this.form.value['id'];
    this.SignosVitales.paciente = this.pacienteSeleccionado;
    this.SignosVitales.fecha = this.form.value['fecha'];
    this.SignosVitales.pulso = this.form.value['pulso'];
    this.SignosVitales.ritmoRespiratorio = this.form.value['ritmo_respiratorio'];
    this.SignosVitales.temperatura = this.form.value['temperatura'];

    if(this.SignosVitales.paciente === undefined){
      this.snackBar.open("Debe seleccionar paciente para guardar datos.", "Aviso", { duration: 2000 });
    } else {
    
    if (this.edicion) {
      //update
      this.SignosVitalesService.modificar(this.SignosVitales).subscribe(data => {
        if (data === 1) {
          this.SignosVitalesService.getlistarSignosVitales(0, 100).subscribe(Signos => {
            this.SignosVitalesService.SignosVitalesCambio.next(Signos);
            this.SignosVitalesService.mensaje.next('Se modificó');
          });
        } else {
          this.SignosVitalesService.mensaje.next('No se modificó');
        }
      });
    } else {
      //insert
      this.SignosVitalesService.registrar(this.SignosVitales).subscribe(data => {
        console.log(data);
        if (data === 1) {
          this.SignosVitalesService.getlistarSignosVitales(0, 100).subscribe(Signos => {
            this.SignosVitalesService.SignosVitalesCambio.next(Signos);
            this.SignosVitalesService.mensaje.next('Se registró');
          });
        } else {
          this.SignosVitalesService.mensaje.next('No se registró');
        }
      });
    }

    this.Router.navigate(['signos-vitales'])
  }
  }

  filter(val: any) {
    if (val != null && val.idPaciente > 0) {
      return this.pacientes.filter(option =>
        option.nombres.toLowerCase().includes(val.nombres.toLowerCase()) || option.apellidos.toLowerCase().includes(val.apellidos.toLowerCase()) || option.dni.includes(val.dni));
    } else {      
      return this.pacientes.filter(option =>
        option.nombres.toLowerCase().includes(val.toLowerCase()) || option.apellidos.toLowerCase().includes(val.toLowerCase()) || option.dni.includes(val));
    }
  }


}
